config_file './config.yml'

LDAP = Net::LDAP.new  :host => settings.ldapHost,
                      :port => 389,
                      :base => settings.ldapBase,
                      :auth => {
                        :method => :simple,
                        :username => "cn=admin,#{settings.ldapBase}",
                        :password => "skjdh435879shSDsdjkgh2837"
}


class Device

  def initialize(hostname, new_guid=nil)
    self.name=(hostname)
    #If GUID not specified, lookup from ldap
    if new_guid.nil?
      #Set GUID from hostname
      filter = Net::LDAP::Filter.eq( "cn", "cn=#{@name}" )
      results = LDAP.search( :filter => filter )
      results.each do |result|
        @guid = result[:serialNumber][0]
      end
    else
      p new_guid
      self.guid=(new_guid)
    end
  end

  def name
    @name
  end

  def name=(name)
    #check if hostname is valid
    if (name =~ /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/).nil?
      #raise "Error: Name entered is not a valid hostname."
    end
    @name = name
  end

  def guid
    @guid
  end

  def guid=(guid)
    begin
      #check if guid is valid
      if (guid =~ /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i).nil?
        raise "Error: GUID entered is not a valid GUID."
      end
      #Check if GUID already used?
      filter = Net::LDAP::Filter.eq( "serialNumber", "#{guid}" )
      results = LDAP.search( :filter => filter )
      results.each do |result|
        raise "Error: GUID already used by #{result[:cn][0]}"
      end
      @guid = guid
    rescue => error
      raise error
    end
  end

  def create
    dn = "cn=#{@name}, ou=Computers, dc=danbishop, dc=org"
    attr = {
      :cn => @name,
      :objectclass => "device",
      :serialNumber => @guid
    }
    p attr
    LDAP.add(:dn => dn, :attributes => attr)
    unless LDAP.get_operation_result.code == 0
      raise "Error: #{LDAP.get_operation_result.error_message}#{LDAP.get_operation_result.message}"
    end
  end

end
