require 'sinatra'
require 'sinatra/config_file'
require 'sinatra/content_for'
require 'net/ldap'
require_relative 'directory.rb'


get '/' do
  erb :index, :layout => :template
end

get '/users' do
  erb :users, :layout => :template
end

get '/groups' do
  erb :groups, :layout => :template
end

get '/computers' do
  erb :computers, :layout => :template
end

post '/computers' do
  begin
    newDevice = Device.new(params[:computerName],params[:guid])
    newDevice.create
  rescue => error
    status 500
    @error = error
    erb :error
  end
end
